"""Lab 2 Task 2
This module contains functions for simulating Brownian motion
and analyzing the results
"""
import numpy as np
import matplotlib.pyplot as plt

def brown1(Nt,M,dt=1):
    """Run M Brownian motion simulations each consisting of Nt time steps
    with time step = dt
    Returns: X: the M trajectories; Xm: the mean across these M samples; Xv:
    the variance across these M samples
    """
    from numpy.random import randn


    #Initialize variable
    X = np.zeros((M,Nt+1))

    #1D Brownian motion: X_j+1 = X_j + sqrt(dt)*N(0,1)
    for i in range(M):
        for j in range(Nt):
            X[i,j+1] = X[i,j] + np.sqrt(dt)*randn(1)

    Xm = np.mean(X,axis=0)
    Xv = np.var(X,axis=0)
    return X,Xm,Xv


def representation(X,Xm,Xv,index = [0,1,2],style = 'b.'):
    time = range(np.shape(X)[1])
    plt.figure()
    lines = [plt.plot(time,X[k,:],style) for k in index]
    lines.append(plt.plot(time,Xm,'-r'))
    lines.append(plt.plot(time,Xv,'-r'))
    #for j in range(len(index)):
    plt.setp(lines[0:len(index)], markersize=0.5)
    plt.setp(lines[-1], linewidth=2)
    plt.setp(lines[-2], linewidth=2)
    plt.title('Graphical reresentation \n of Brownian Motion')
    #plt.legend(lines,'12345')
    #plt.legend(tuple(lines[-2]),('Moyenne'))
    #plt.show()
    #return type(lines)





def analyze(display=False,M_end = 100,Nt=100,display_lin=False,display_log = False):
    """Function to analyze simulation error
    """
    error_vec = [];
    variance_vec = [];

    X,Xm,Xv = brown1(Nt,M_end,dt=1)

    for M in range(1,M_end+1):
        X_temp = X[0:M,:]
        Xvv = np.var(X_temp,axis=0)
        error_vec.append(np.abs(Xvv[Nt]-Nt))
        variance_vec.append(Xvv[Nt])

    M_values = [range(1,M_end+1)]

    if display_lin:
        plt.figure();
        plt.plot(np.transpose(M_values),error_vec);
        plt.title('Variance at t=100 \n for several values of M')
        plt.show()

    if display_log:
        plt.figure();
        plt.loglog(np.transpose(M_values),error_vec);
        plt.plot(np.transpose(M_values),np.transpose(np.divide(np.ones(M_end),M_values)))#,np.exp(M_values))
        #plt.loglog(np.transpose(M_values),np.exp(np.divide(np.ones(M_end,1),exp(M_values))))
        #plt.title('Variance at t=100 \n for several values of M (log-log plot)')
        #plt.show()
        #return M_values


    #return M_values,error_vec,variance_vec
